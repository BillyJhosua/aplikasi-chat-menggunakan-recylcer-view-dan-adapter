package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    MessageAdapter messageAdapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Message> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Message message1 = new Message();
        message1.setName("James");
        message1.setMessage("Thank you! That was very helpful!");
        message1.setImage(R.drawable.james);

        Message message2 = new Message();
        message2.setName("Will Kenny");
        message2.setMessage("I know... I'm trying to get the funds.");
        message2.setImage(R.drawable.willkenny);

        Message message3 = new Message();
        message3.setName("Beth Williams");
        message3.setMessage("I'm looking for tips around capturing the milky way. I have a 6D with a 24-100mm");
        message3.setImage(R.drawable.bethwilliam);

        Message message4 = new Message();
        message4.setName("Rev Shawn");
        message4.setMessage("Wanted to ask if you're available for a portrait shoot next week");
        message4.setImage(R.drawable.revshawn);

        arrayList.add(message1);
        arrayList.add(message2);
        arrayList.add(message3);
        arrayList.add(message4);

        messageAdapter = new MessageAdapter(this, arrayList);
        recyclerView = findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(messageAdapter);
    }
}
